from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'bonzafe.views.home', name='home'),
	url(r'^login/', include('login.urls')),
	url(r'^admin/',include(admin.site.urls)),
)
urlpatterns += static(settings.MEDIA_URL,
document_root=settings.MEDIA_ROOT)