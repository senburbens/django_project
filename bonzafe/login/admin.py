from django.contrib import admin
from login.models import Categories, Produits

admin.site.register(Categories)
admin.site.register(Produits)