#-*- coding: utf-8 -*-
from django.shortcuts import render
from login.models import *
from login.forms import *
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.shortcuts import redirect
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout 


def acceuil(request):
	derniers_produits=Produits.objects.order_by('-id')[: 10]
	return render(request,'login/index.html',{'derniers_produits':derniers_produits})
	
def page_inscription(request):
	if request.method == 'POST':
		error=False
		userExist=False
		form=InscriptionForm(request.POST)
		if form.is_valid():			
			username1=form.cleaned_data['username']
			email=form.cleaned_data['email']
			password1=form.cleaned_data['password']
			confirmation=form.cleaned_data['confirmation']
			users=User.objects.all()
			for user in  users:
				if user.username==username1:
					userExist=True
					break				
			if not userExist:
				user_to_register=User(username=username1,email=email,password=password1)
				user_to_register.set_password(password1)
				user_to_register.save()
				return redirect(reverse(connexion))
			else :
				error=True
	else:
		form=InscriptionForm()			
	return render(request, 'login/inscription.html', locals())	
	

def deconnexion(request):
	if request.user.is_authenticated():
		logout(request)                                                                     
	return redirect(reverse(connexion))
	
def termes(request):
	return render(request, 'login/termes.html')

def connexion(request):
	if request.method == 'POST':
		form = LoginForm(request.POST)
		if form.is_valid(): 
			username = form.cleaned_data['username']
			password = form.cleaned_data['password']
			user=authenticate(username=username,password=password)
			if user :
				login(request,user)
				return redirect(reverse(page_perso))
			else :
				error=True
	else:
		form = LoginForm()
	return render(request, 'login/connexion.html', locals())
	
def ajouter_produit(request):
	produit_enregistre=False
	if request.method == 'POST':
		form = AjouterProduitForm(request.POST, request.FILES)
		if form.is_valid():
			nom=form.cleaned_data['nom']
			description=form.cleaned_data['description']
			prix=form.cleaned_data['prix']
			categorie=form.cleaned_data['categorie']
			image=form.cleaned_data['image']
			Produits(nom=nom,description=description,prix=prix,user=request.user,categorie=categorie,image=image).save()
			produit_enregistre=True
	else:
		form = AjouterProduitForm()
	return render(request, 'login/ajouter_produit.html', locals())

def voir_produit(request, id):
	produit=Produits.objects.get(id=id)
	return render(request,'login/voir_produit.html',locals())

def contacts(request):
	return render(request,'login/page_contacts.html',locals())
	

def page_perso(request):
	if request.user.is_authenticated():
		# derniers_produits=Produits.objects.order_by('-id')[: 10]
		derniers_produits=Produits.objects.filter(user__username=request.user.username).order_by('-id')[: 10]
		return render(request,'login/page_perso.html',locals())
	else :
		return redirect(reverse(connexion))
		
def rechercher_produits(request):
	produits_trouves=False
	pas_de_produits=False
	if request.method=='POST':
		form=RechercherForm(request.POST)
		if form.is_valid():
			nom_produit=form.cleaned_data['nomProduit']
			produits=Produits.objects.filter(nom__contains=nom_produit).filter(user__username=request.user.username)
			if produits :
				produits_trouves=True
			else :
				pas_de_produits=True
	else :
		form=RechercherForm()
	return render(request,'login/rechercher_produit.html',locals())
	
def supprimer_produit(request, id):
	produit=Produits.objects.filter(id=id)
	produit.delete()
	produit_supprime=True
	return redirect(reverse(rechercher_produits))
	
def modifier_produit(request,id):
	produit_modifier=False
	produit=Produits.objects.get(id=id)
	if request.method == 'POST':
		form = AjouterProduitForm(request.POST, request.FILES)
		if form.is_valid():
			nom=form.cleaned_data['nom']
			description=form.cleaned_data['description']
			prix=form.cleaned_data['prix']
			categorie=form.cleaned_data['categorie']
			image=form.cleaned_data['image']
			
			produit.nom=nom
			produit.description=description
			produit.prix=prix
			produit.user=request.user
			produit.categorie=categorie
			produit.image=image
			produit.save()
			
			produit_modifie=True
	else:
		form = AjouterProduitForm()
	return render(request, 'login/modifier_produit.html', locals())
	
def mode_acheteur(request):
	if request.user.is_authenticated():
		return render(request,'login/shopping.html',locals())
	else:
		return render(request,'login/acceuil.html',locals())
		
def shopping(request):
	produit_non_trouve=True
	if request.method=='POST':
		form=ShoppingForm(request.POST)
		if form.is_valid():
			categorie=form.cleaned_data['categorie']
			produits=Produits.objects.filter(categorie__categorie=categorie)
			produit_non_trouve=False
	else:
		form=ShoppingForm()
	return render(request,'login/shopping.html',locals())
