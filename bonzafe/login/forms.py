#-*- coding: utf-8 -*-
from django import forms
from login.models import *

TYPE=(('NOM','Nom'),('CATEGORIE','Categorie'),)

class InscriptionForm(forms.Form):
	username=forms.CharField(max_length=100, required=True)
	email=forms.EmailField(label=u"Adresse mail", required=True)
	password=forms.CharField(widget=forms.PasswordInput, required=True)
	confirmation=forms.CharField(widget=forms.PasswordInput, required=True)
	
	def clean(self):
		cleaned_data=super(InscriptionForm,self).clean()
		password=cleaned_data.get('password')
		confirmation=cleaned_data.get('confirmation')
		if password and confirmation :
			if password != confirmation :
				raise forms.ValidationError("Les mots de passe entres ne sont pas identiques")
		return cleaned_data

class LoginForm(forms.Form):
    username = forms.CharField(label=u"Username", max_length=100, required=True)
    password = forms.CharField(widget=forms.PasswordInput, required=True)

class AjouterProduitForm(forms.ModelForm):
	class Meta:
		model=Produits
		exclude=('user',)

class RechercherForm(forms.Form):
	nomProduit=forms.CharField(label=u"Nom du produit", max_length=100, required=True)
	
class ShoppingForm(forms.Form):
	categorie=forms.CharField(label=u"Categorie",max_length=100, required=True)