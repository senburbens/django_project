#-*- coding: utf-8 -*-
from django.conf.urls import patterns, url


urlpatterns = patterns('login.views',
    url(r'^$','acceuil'),
	url(r'^page_inscription/$','page_inscription'),
	url(r'^termes/$','termes'),
	url(r'^connexion/$','connexion'),
	url(r'^page_perso/$','page_perso'),
	url(r'^deconnexion/$','deconnexion'),
	url(r'^ajouter_produit/$','ajouter_produit'),
	url(r'^voir_produit/(\d+)/$','voir_produit'),
	url(r'^page_contacts/$','contacts'),
	url(r'^rechercher_produits/$','rechercher_produits'),
	url(r'^supprimer_produits/(\d+)/$','supprimer_produit'),
	url(r'^modifier_produits/(\d+)/$','modifier_produit'),
	url(r'^mode_acheteur/$','mode_acheteur'),
	url(r'^shopping/$','shopping'),
	)