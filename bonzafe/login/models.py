#-*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User

	
class Categories(models.Model):
	categorie= models.CharField(max_length=200)
	
	def __str__(self):
		return self.categorie
 
class Produits(models.Model):
	nom = models.CharField(max_length=200)
	description = models.CharField(max_length=200)
	prix = models.IntegerField()
	user = models.ForeignKey(User)
	categorie = models.ForeignKey(Categories)
	image=models.ImageField(upload_to="produits/")
	

	
	
